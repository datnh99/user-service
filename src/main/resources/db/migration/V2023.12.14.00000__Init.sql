# Init user-service database

-- CREATE TABLE users --
CREATE TABLE IF NOT EXISTS users
(
    id            INT AUTO_INCREMENT PRIMARY KEY,
    username      char(20) UNIQUE NOT NULL,
    pass          char(20)        NOT NULL,
    email         char(50) UNIQUE NOT NULL,
    address       char(200),
    created_date  DATETIME,
    modified_date DATETIME
);

-- CREATE TABLE roles --
CREATE TABLE IF NOT EXISTS roles
(
    id            INT AUTO_INCREMENT PRIMARY KEY,
    name          char(20) UNIQUE NOT NULL,
    description   char(200),
    created_date  DATETIME,
    modified_date DATETIME
);

-- CREATE TABLE user_role --
CREATE TABLE IF NOT EXISTS user_role
(
    user_id       INT NOT NULL,
    role_id       INT NOT NULL,
    is_active     BIT NOT NULL,
    created_date  DATETIME,
    modified_date DATETIME,
    PRIMARY KEY (user_id, role_id),
    FOREIGN KEY (user_id) REFERENCES users (id),
    FOREIGN KEY (role_id) REFERENCES roles (id)
);

-- CREATE TABLE login_infors --
CREATE TABLE IF NOT EXISTS login_infors
(
    id            INT AUTO_INCREMENT PRIMARY KEY,
    user_id       INT      NOT NULL,
    ip_address    char(20) NOT NULL,
    login_time    DATETIME,
    modified_date DATETIME,
    FOREIGN KEY (user_id) REFERENCES users (id)
);

-- CREATE TABLE user_details --
CREATE TABLE IF NOT EXISTS user_details
(
    user_id       INT PRIMARY KEY,
    first_name    char(20) NOT NULL,
    last_name     char(20),
    phone_number  char(10) NOT NULL,
    gender        BIT      NOT NULL,
    dob           DATETIME NOT NULL,
    created_date  DATETIME,
    modified_date DATETIME,
    FOREIGN KEY (user_id) REFERENCES users (id)
);

-- CREATE TABLE purchase_histories --
CREATE TABLE IF NOT EXISTS purchase_histories
(
    id            INT AUTO_INCREMENT PRIMARY KEY,
    user_id       INT      NOT NULL,
    order_id      INT      NOT NULL,
    purchase_date DATETIME NOT NULL,
    amount        double   NOT NULL,
    discount      double   NOT NULL,
    FOREIGN KEY (user_id) REFERENCES users (id)
);

-- CREATE TABLE cart --
CREATE TABLE IF NOT EXISTS cart
(
    id             INT AUTO_INCREMENT PRIMARY KEY,
    user_id        INT    NOT NULL,
    product_id     INT    NOT NULL,
    product_number INT    NOT NULL,
    price          double NOT NULL,
    discount       double NOT NULL,
    FOREIGN KEY (user_id) REFERENCES users (id)
);

-- CREATE TABLE address_type --
CREATE TABLE IF NOT EXISTS address_type
(
    id           INT AUTO_INCREMENT PRIMARY KEY,
    address_type char(20) UNIQUE NOT NULL,
    description  char(200)
);

-- CREATE TABLE shipping_addresses --
CREATE TABLE IF NOT EXISTS shipping_addresses
(
    id            INT AUTO_INCREMENT PRIMARY KEY,
    user_id       INT       NOT NULL,
    address       char(200) NOT NULL,
    is_default    BIT       NOT NULL,
    type_id       INT       NOT NULL,
    note          char(200),
    created_date  DATETIME,
    modified_date DATETIME,
    FOREIGN KEY (user_id) REFERENCES users (id),
    FOREIGN KEY (type_id) REFERENCES address_type (id)
);

