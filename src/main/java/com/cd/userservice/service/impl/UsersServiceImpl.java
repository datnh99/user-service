package com.cd.userservice.service.impl;

import com.cd.userservice.data.entity.Users;
import com.cd.userservice.exception.NullPointerCustomerException;
import com.cd.userservice.repository.UsersRepository;
import com.cd.userservice.service.UsersService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@RequiredArgsConstructor
public class UsersServiceImpl implements UsersService {

    private final UsersRepository usersRepository;

    @Override
    public Optional<Users> findById(Integer id) {
        return Optional.ofNullable(usersRepository.findById(id)
                .orElseThrow(() -> new NullPointerCustomerException("User not found with username: " + id)));
    }
}
