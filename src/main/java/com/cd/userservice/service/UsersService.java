package com.cd.userservice.service;

import com.cd.userservice.data.entity.Users;

import java.util.Optional;

public interface UsersService {
    Optional<Users> findById(Integer id);
}
