package com.cd.userservice.exception;

public class NullPointerCustomerException extends RuntimeException{

    public NullPointerCustomerException(String message) {
        super(message);
    }
}
