package com.cd.userservice.controller;

import com.cd.userservice.data.entity.Users;
import com.cd.userservice.service.UsersService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/users")
@RequiredArgsConstructor
public class UsersController {

    @Value("${host}")
    private String host;

    private final UsersService usersService;

    @GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Users> findById(@PathVariable Integer id, @RequestParam(required = false) String a) {
        return ResponseEntity.of(usersService.findById(id));
    }
}
