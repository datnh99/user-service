package com.cd.userservice.data.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Builder
public class Users {
    @Id
    private Integer id;

    @Column(length = 20, nullable = false)
    private String username;
    @Column(length = 20, nullable = false)
    private String pass;
    @Column(length = 50, nullable = false)
    private String email;
    @Column(length = 200)
    private String address;
}
