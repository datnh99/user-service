package com.cd.userservice.data.base;

import java.time.LocalDateTime;

public class BaseEntity {
    private LocalDateTime createdDate;
    private LocalDateTime modifiedDate;
}
