package com.cd.userservice.repository;

import com.cd.userservice.data.entity.Users;
import io.micrometer.common.lang.NonNullApi;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
@NonNullApi
public interface UsersRepository extends JpaRepository<Users, Integer> {

    Optional<Users> findById(Integer id);

}
