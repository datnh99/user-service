package com.cd.userservice.service.impl;

import com.cd.userservice.data.entity.Users;
import com.cd.userservice.exception.NullPointerCustomerException;
import com.cd.userservice.repository.UsersRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class UsersServiceImplTest {

    @Mock
    private UsersRepository usersRepository;

    @InjectMocks
    private UsersServiceImpl usersService;

    @Test
    void findById() {
        Users users = Users.builder()
                .id(1)
                .username("username")
                .pass("pass")
                .email("email@gmail.com")
                .address("address")
                .build();
        when(usersRepository.findById(1)).thenReturn(Optional.of(users));

        Users expected = usersService.findById(1).orElse(null);

        assertNotNull(expected);
        assertEquals(expected.getId(), users.getId());
    }

    @Test
    void findById_whenIdNotFound() {
        when(usersRepository.findById(1)).thenReturn(Optional.empty());

        assertThrows(NullPointerCustomerException.class, () -> {
            usersService.findById(1);
        });
    }
}